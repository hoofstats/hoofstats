"""
Installation file for hoofstats.
"""
import os
from setuptools import setup

srcdir = os.path.dirname(__file__)
readme_filename = os.path.join(srcdir, 'README.md')

def read_version_py(filename='hoofstats/version.py'):
    ns = {}
    with open(filename, 'rb') as fd:
        exec(fd.read(), ns)
    return ns['__version__']

version = read_version_py()

setup(
    name='hoofstats',
    version=version,
    description='Hoof microscribe data statistics.',
    long_description=open(readme_filename, encoding="utf-8").read(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3',
    ],
    keywords='hoof geometry, microscribe, measurements, statistics',
    url='https://gitlab.com/hoofstats/hoofstats',
    author='Robert Cimrman',
    author_email='cimrman3@ntc.zcu.cz',
    license='BSD',
    packages=['hoofstats'],
    python_requires='>=3.6',
    install_requires=[
        'matplotlib',
        'numpy',
        'openpyxl',
    ],
    entry_points={
        'console_scripts': [
            'hoofstats=hoofstats.hoofstats:main',
        ],
    },
    include_package_data=True,
    zip_safe=False,
)
