import os

basestr = str

class Struct(dict):
    """
    A dict with the attribute access to its items.
    """

    def __getattr__(self, name):
        try:
            return self[name]

        except KeyError:
            raise AttributeError(name)

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __str__(self):
        if self.keys():
            return '\n'.join(
                [self.__class__.__name__ + ':'] +
                self.format_items()
            )

        else:
            return self.__class__.__name__

    def __repr__(self):
        return self.__class__.__name__

    def __dir__(self):
        return list(self.keys())

    def copy(self):
        return type(self)(self)

    def format_items(self):
        num = max(map(len, list(self.keys()))) + 1
        return [key.rjust(num) + ': ' + repr(val)
                for key, val in sorted(self.items())]

class Output(Struct):
    """
    A class that provides output (print) functions.
    """

    def __init__(self, prefix, filename=None, quiet=False, combined=False,
                 append=False, **kwargs):
        Struct.__init__(self, **kwargs)

        self.prefix = prefix

        self.set_output(filename=filename, quiet=quiet,
                        combined=combined, append=append)

    def __call__(self, *argc, **argv):
        """
        Call `self.output_function()`.

        Parameters
        ----------
        argc : positional arguments
            The values to print.
        argv : keyword arguments
            The arguments to control the output behaviour. Supported keywords
            are listed below.
        verbose : bool (in **argv)
            No output if False.
        """
        verbose = argv.get('verbose', True)
        if verbose:
            self.output_function(*argc, **argv)

    def get_prefix(self):
        if len(self.prefix):
            return self.prefix + ' ' + ('  ' * self.level)

        else:
            return self.prefix + ('  ' * self.level)

    def set_output(self, filename=None, quiet=False, combined=False,
                   append=False):
        """
        Set the output mode.

        If `quiet` is `True`, no messages are printed to screen. If
        simultaneously `filename` is not `None`, the messages are logged
        into the specified file.

        If `quiet` is `False`, more combinations are possible. If
        `filename` is `None`, output is to screen only, otherwise it is
        to the specified file. Moreover, if `combined` is `True`, both
        the ways are used.

        Parameters
        ----------
        filename : str or file object
            Print messages into the specified file.
        quiet : bool
            Do not print anything to screen.
        combined : bool
            Print both on screen and into the specified file.
        append : bool
            Append to an existing file instead of overwriting it. Use with
            `filename`.
        """
        if not isinstance(filename, basestr):
            # filename is a file descriptor.
            append = True

        self.level = 0

        def output_none(*argc, **argv):
            pass

        def output_screen(*argc, **argv):
            format = '%s' + ' %s' * (len(argc) - 1)
            msg = format % argc

            if msg.startswith('...'):
                self.level -= 1

            print(self.get_prefix() + msg)

            if msg.endswith('...'):
                self.level += 1

        def print_to_file(filename, msg):
            if isinstance(filename, basestr):
                fd = open(filename, 'a')

            else:
                fd = filename

            print(self.get_prefix() + msg, file=fd)

            if isinstance(filename, basestr):
                fd.close()

            else:
                fd.flush()

        def output_file(*argc, **argv):
            format = '%s' + ' %s' * (len(argc) - 1)
            msg = format % argc

            if msg.startswith('...'):
                self.level -= 1

            print_to_file(filename, msg)

            if msg.endswith('...'):
                self.level += 1

        def output_combined(*argc, **argv):
            format = '%s' + ' %s' * (len(argc) - 1)
            msg = format % argc

            if msg.startswith('...'):
                self.level -= 1

            print(self.get_prefix() + msg)

            print_to_file(filename, msg)

            if msg.endswith('...'):
                self.level += 1

        def reset_file(filename):
            if isinstance(filename, basestr):
                output_dir = os.path.dirname(filename)
                if output_dir and not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                fd = open(filename, 'w')
                fd.close()

            else:
                raise ValueError('cannot reset a file object!')

        if quiet is True:
            if filename is not None:
                if not append:
                    reset_file(filename)

                self.output_function = output_file

            else:
                self.output_function = output_none

        else:
            if filename is None:
                self.output_function = output_screen

            else:
                if not append:
                    reset_file(filename)

                if combined:
                    self.output_function = output_combined

                else:
                    self.output_function = output_file

    def get_output_function(self):
        return self.output_function

output = Output('')
