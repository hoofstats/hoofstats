import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

def get_axes(ax, dim):
    if ax is None:
        fig = plt.figure()
        if dim == 3:
            from mpl_toolkits.mplot3d import axes3d
            axes3d # Make pyflakes happy...

            ax = fig.add_subplot(111, projection='3d')

        else:
            ax = fig.add_subplot(111)

    return ax

def show():
    plt.show()

def put_legend_to_figure(fig, ax_in):
    plt.figure(fig.number)
    aa = plt.figlegend(*ax_in.get_legend_handles_labels(), loc='upper left')
    fig.axes.clear()
    fig.axes.append(aa)
    return aa

def plot_points(ax, coors):
    ax = get_axes(ax, coors.shape[1])
    ax.scatter(*coors.T)

    return ax

def plot_point_names(ax, coors, names, **kwargs):
    ax = get_axes(ax, coors.shape[1])
    for ii, name in enumerate(names):
        ax.text(*coors[ii], name, **kwargs)

    return ax

def plot_measures(ax, measures, fontsize=10):
    ax = get_axes(ax, 3)

    colors = plt.cm.viridis(np.linspace(0, 1, len(measures)))
    ax.set_prop_cycle(
        plt.cycler('color', colors)
    )

    for name, measure in measures.items():
        desc = measure[0]
        if desc in ('line', 'segment'):
            p0, p1 = measure[2], measure[3]
            ax.plot(*zip(p0, p1), label=f'|{name}| = {measure[-1]:.2f}')
            ax.text(*(0.7 * p0 + 0.3 * p1), name, fontsize=fontsize)

        elif desc == 'point':
            pp = measure[2]
            aux = tuple([[ii] for ii in pp])
            ax.plot(*aux, ls='None', marker='x', label=f'point {name}')
            ax.text(*pp, name, fontsize=fontsize)

        elif desc == 'plane':
            p0, p1, p2 = measure[2], measure[3], measure[4]
            poly = Poly3DCollection([(p0, p1, p2)], alpha=0.1)
            ax.plot(*zip(p0, p1, p2, p0), label=f'plane {name}')
            ax.add_collection3d(poly)
            ax.text(*((p0 + p1 + p2) / 3.0), name, fontsize=fontsize)

        elif desc == 'polygon':
            ps = measure[2]
            ax.plot(*ps.T, label=f'|{name}| = {measure[-1]:.2f}')
            ax.text(*ps[0], name, fontsize=fontsize)

        elif desc == 'angle':
            pm0, pm1 = measure[2], measure[3]
            ax.plot(*zip(pm0, pm1), ls='--',
                    label=f'<{name} = {measure[-1]:.2f}')
            ax.text(*(0.5 * pm0 + 0.5 * pm1), name, fontsize=fontsize)

        elif desc == 'area':
            pc, ps = measure[2], measure[3]
            for ii in range(ps.shape[0] - 1):
                p0, p1 = ps[ii], ps[ii+1]
                if ii == 0:
                    line, = ax.plot(*zip(pc, p0), ls=':',
                                    label=f'||{name}|| = {measure[-1]:.2f}')
                    color = line.get_color()

                else:
                    ax.plot(*zip(pc, p0), color=color, ls=':')

                poly = Poly3DCollection([(pc, p0, p1)], color=color,
                                        edgecolor='None', alpha=0.1)
                ax.add_collection3d(poly)
            ax.text(*pc, name, fontsize=fontsize)

    return ax
