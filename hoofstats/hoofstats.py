#!/usr/bin/env python
"""
Process microscribe data.
"""
from argparse import ArgumentParser
import os
import sys
import datetime
import numpy as np

from openpyxl import load_workbook, Workbook
from openpyxl.utils import range_boundaries

from hoofstats.base import Struct, output

default_data_description = {
    'date' : 'B2',
    'session' : 'D4',
    'project' : 'D5',
    'point_desc_cols' : 'A{}:C{}',
    'point_coors_cols' : 'D{}:F{}',
}

class Info(Struct):
    """
    Data information.
    """

def get_row(ws, rng):
    if ':' in rng:
        data = [cell.value for cell in ws[rng][0]]

    else:
        data = ws[rng].value

    return data

def get_info(ws, desc):
    info = Info(**{key: get_row(ws, desc[key])
                   for key in ['date', 'session', 'project']})
    return info

def get_points(ws, desc, rows):
    pdesc_range = desc['point_desc_cols'].format(*rows)
    pdesc = []
    a, b, c, d = range_boundaries(pdesc_range)
    for row in ws.iter_rows(min_col=a, min_row=b, max_col=c, max_row=d,
                            values_only=True):
        if row[2] is None:
            continue
        pdesc.append(row)

    pcoors_range = desc['point_coors_cols'].format(*rows)
    pcoors = []
    a, b, c, d = range_boundaries(pcoors_range)
    for row in ws.iter_rows(min_col=a, min_row=b, max_col=c, max_row=d,
                            values_only=True):
        if None in row:
            continue
        pcoors.append(row)

    pdesc = np.array(pdesc)
    pcoors = np.array(pcoors)

    if pdesc.shape != pcoors.shape:
        raise ValueError(f'point data shape mismatch (description x coors!'
                         f' ({pdesc.shape} == {pcoors.shape})')

    return pdesc, pcoors

def _get_single(arr, msg):
    if len(arr) != 1:
        raise ValueError(msg)

    return arr[0]

def _get_item(name, index, pcoors, measures, kind=None):
    item = measures.get(name, None)
    if item is None:
        ir = _get_single(np.where(index == name)[0],
                         f'item {name} not found!')
        item = pcoors[ir]

    elif kind == 'point':
        # The first point defining a line.
        item = item[2]

    return item

def get_angle_vec_vec(v0, v1):
    # angle = (180.0 / np.pi) * np.arccos(np.dot(v0 / d0, v1 / d1))
    # This should be more stable.
    sina = np.linalg.norm(np.cross(v0, v1))
    cosa = np.dot(v0, v1)
    angle = (180.0 / np.pi) * np.arctan2(sina, cosa)

    return angle

def eval_measures(pdesc, pcoors, measures_description):
    measures = {}
    index = pdesc[:, 2]
    for name, desc in measures_description.items():
        output(name, desc)

        if desc[0] in ('line', 'segment'):
            p0 = _get_item(desc[1], index, pcoors, measures, kind='point')
            p1 = _get_item(desc[2], index, pcoors, measures, kind='point')
            measures[name] = (desc[0], name, p0, p1, p1 - p0,
                              np.linalg.norm(p1 - p0))

        elif desc[0] == 'plane':
            p0 = _get_item(desc[1], index, pcoors, measures, kind='point')
            p1 = _get_item(desc[2], index, pcoors, measures, kind='point')
            p2 = _get_item(desc[3], index, pcoors, measures, kind='point')
            n = np.cross(p1 - p0, p2 - p0)
            nn = np.linalg.norm(n)
            if nn < 1e-10:
                raise ValueError(f'points {desc[1]}, {desc[2]}, {desc[3]}'
                                 f' are colinear!')
            n /= nn
            measures[name] = (desc[0], name, p0, p1, p2, n)

        elif desc[0] == 'nearest-points':
            # See https://en.wikipedia.org/wiki/Skew_lines#Nearest_Points
            line0 = _get_item(desc[1], index, pcoors, measures)
            line1 = _get_item(desc[2], index, pcoors, measures)

            p0, v0 = line0[2], line0[-2]
            p1, v1 = line1[2], line1[-2]

            n01 = np.cross(v0, v1)
            nn01 = np.linalg.norm(n01)
            if nn01 < 1e-10:
                raise ValueError(f'lines {desc[1]} and {desc[2]} are parallel!')

            n01 /= nn01
            dist = np.abs(np.dot(n01, line0[2] - line1[2]))
            output(f'line distance: {dist:.2f}')

            n1 = np.cross(n01, v1)
            cp0 = p0 + (np.dot(p1 - p0, n1) / np.dot(v0, n1)) * v0

            n0 = np.cross(n01, v0)
            cp1 = p1 + (np.dot(p0 - p1, n0) / np.dot(v1, n0)) * v1
            dist2 = np.linalg.norm(cp1 - cp0)
            assert abs(dist2 - dist) < 1e-12

            measures[name] = ('segment', name, cp0, cp1, cp1 - cp0, dist)

        elif desc[0] == 'intersect-line-plane':
            # See https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
            line = _get_item(desc[1], index, pcoors, measures)
            plane = _get_item(desc[2], index, pcoors, measures)

            p0, v0 = line[2], line[-2]
            p1, n1 = plane[2], plane[-1]
            pip = p0 + (np.dot(p1 - p0, n1) / np.dot(v0, n1)) * v0

            measures[name] = ('point', name, pip)

        elif desc[0] == 'perpendicular-to-plane':
            p0 = _get_item(desc[1], index, pcoors, measures, kind='point')
            plane = _get_item(desc[2], index, pcoors, measures)

            p1, n1 = plane[2], plane[-1]
            pip = p0 + np.dot(p1 - p0, n1) * n1

            measures[name] = ('segment', name, pip, p0, p0 - pip,
                              np.linalg.norm(p0 - pip))

        elif desc[0] == 'perpendicular-to-line':
            p0 = _get_item(desc[1], index, pcoors, measures, kind='point')
            line = _get_item(desc[2], index, pcoors, measures)

            p1, v1, d1 = line[2], line[-2], line[-1]
            if d1 < 1e-10:
                raise ValueError(f'line {desc[2]} has zero length!')
            v1 /= d1

            pil = p1 + np.dot(p0 - p1, v1) * v1

            measures[name] = ('segment', name, pil, p0, p0 - pil,
                              np.linalg.norm(p0 - pil))

        elif desc[0] == 'polygon':
            ps = np.array([_get_item(ii, index, pcoors, measures, kind='point')
                           for ii in desc[1:]])
            lengths = np.linalg.norm(np.diff(ps, axis=0), axis=1)

            measures[name] = ('polygon', name, ps, lengths, lengths.sum())

        elif desc[0] == 'point':
            p0 = _get_item(desc[1], index, pcoors, measures, kind='point')

            measures[name] = ('point', name, p0)

        elif desc[0] == 'angle-line-line':
            line0 = _get_item(desc[1], index, pcoors, measures)
            line1 = _get_item(desc[2], index, pcoors, measures)
            v0 = line0[-2]
            v1 = line1[-2]
            angle = get_angle_vec_vec(v0, v1)

            pm0 = np.average(line0[2:4], axis=0)
            pm1 = np.average(line1[2:4], axis=0)

            measures[name] = ('angle', name, pm0, pm1, angle)

        elif desc[0] == 'angle-line-plane':
            line = _get_item(desc[1], index, pcoors, measures)
            plane = _get_item(desc[2], index, pcoors, measures)
            v0 = line[-2]
            n1 = plane[-1]

            angle = 90 - get_angle_vec_vec(v0, n1)

            pm0 = np.average(line[2:4], axis=0)
            pm1 = np.average(plane[2:5], axis=0)

            measures[name] = ('angle', name, pm0, pm1, angle)

        elif desc[0] == 'angle-plane-plane':
            plane0 = _get_item(desc[1], index, pcoors, measures)
            plane1 = _get_item(desc[2], index, pcoors, measures)
            n0 = plane0[-1]
            n1 = plane1[-1]

            angle = get_angle_vec_vec(n0, n1)

            pm0 = np.average(plane0[2:5], axis=0)
            pm1 = np.average(plane1[2:5], axis=0)

            measures[name] = ('angle', name, pm0, pm1, angle)

        elif desc[0] == 'area-of-polygon':
            polygon = _get_item(desc[1], index, pcoors, measures)
            ps = polygon[2]

            pc = np.average(ps, axis=0)
            areas = []
            for ii in range(ps.shape[0] - 1):
                p0, p1 = ps[ii], ps[ii+1]
                area = 0.5 * np.linalg.norm(np.cross(p0 - pc, p1 - pc))
                areas.append(area)

            areas = np.array(areas)
            measures[name] = ('area', name, pc, ps, areas, areas.sum())

        else:
            raise ValueError(f'unknown measure kind! ({desc[0]})')

    return measures

def output_measures(output, measures):
    pos = 30
    def _format(name, value):
        return name.rjust(pos) + ' = ' + value

    for name, measure in measures.items():
        desc = measure[0]
        if desc in ('line', 'segment'):
            output(_format(f'|{name}|', f'{measure[-1]}'))

        elif desc == 'point':
            output(_format(f'point {name} position', f'{measure[2]}'))

        elif desc == 'plane':
            output(_format(f'plane {name} normal', f'{measure[-1]}'))

        elif desc == 'polygon':
            output(_format(f'|{name}|', f'{measure[-1]}'))

        elif desc == 'angle':
            output(_format(f'<{name}', f'{measure[-1]}'))

        elif desc == 'area':
            output(_format(f'||{name}||', f'{measure[-1]}'))

def save_measures(filename, measures, measures_description):
    wb = Workbook()
    ws = wb.active

    ws.title = 'measures'

    ws.append(['created: %s' % datetime.datetime.now()])
    ws.append([])
    ws.append(['measure', 'definition', 'kind', 'value'])

    for name, measure in measures.items():
        kind = measure[0]
        desc = measures_description[name]

        if kind in ('line', 'segment', 'polygon', 'angle', 'area', 'plane'):
            val = measure[-1]

        elif kind == 'point':
            val = measure[2]

        else:
            raise ValueError(f'unknown measure kind! ({kind})')

        if not np.isscalar(val):
            val = str(val)

        row = [name, str(desc), kind, val]
        ws.append(row)

    dims = {}
    for row in ws.rows:
        for ic, cell in enumerate(row):
            if cell.value:
                dims[cell.column_letter] = max((dims.get(cell.column, 0),
                                                len(str(cell.value))))

    for col, value in dims.items():
        ws.column_dimensions[col].width = 2 * value

    wb.save(filename)

helps = {
    'filename': 'microscribe data file name',
    'worksheet' : 'worksheet name [default: first present]',
    'rows' : 'point data rows range [default: %(default)s]',
    'measures' : 'measures description file',
    'select_plot_measures' : 'measures to plot. Plot all if not given.',
    'plot_point_names' : 'add point names to 3D plot',
    'separate_legend' : 'put legend into separate figure',
    'graphics' : """graphics backend. If vedo, press 's' to save a screenshot
                    of the current view [default: %(default)s]""",
    'no_show' :
    'do not show the figures',
    'silent' : 'do not print messages to screen',
    'clear' :
    'clear old solution files from output directory',
    'output_dir' :
    'output directory [default: %(default)s]',
}

def main():
    parser = ArgumentParser(description=__doc__.rstrip())
    parser.add_argument('filename', help=helps['filename'])
    parser.add_argument('-w', '--worksheet',  metavar='worksheet-name',
                        action='store', dest='worksheet',
                        default=None, help=helps['worksheet'])
    parser.add_argument('-r', '--rows', metavar='rows-spec',
                        action='store', dest='rows',
                        default='8:', help=helps['rows'])
    parser.add_argument('-m', '--measures', metavar='filename',
                        action='store', dest='measures',
                        default=None, help=helps['measures'])
    parser.add_argument('--select-plot-measures', metavar='name1[,name2,...]',
                        action='store', dest='select_plot_measures',
                        default=None, help=helps['select_plot_measures'])
    parser.add_argument('--plot-point-names',
                        action='store_true', dest='plot_point_names',
                        default=False, help=helps['plot_point_names'])
    parser.add_argument('--separate-legend',
                        action='store_true', dest='separate_legend',
                        default=False, help=helps['separate_legend'])
    parser.add_argument('--graphics', choices=['matplotlib', 'vedo'],
                        action='store', dest='graphics',
                        default='matplotlib', help=helps['graphics'])
    parser.add_argument('-n', '--no-show',
                        action='store_false', dest='show',
                        default=True, help=helps['no_show'])
    parser.add_argument('--silent',
                        action='store_false', dest='verbose',
                        default=True, help=helps['silent'])
    parser.add_argument('-c', '--clear',
                        action='store_true', dest='clear',
                        default=False, help=helps['clear'])
    parser.add_argument('-o', '--output-dir', metavar='path',
                        action='store', dest='output_dir',
                        default='output', help=helps['output_dir'])

    options = parser.parse_args()

    output_dir = options.output_dir
    output.set_output(filename=os.path.join(output_dir,'output_log.txt'),
                      combined=options.verbose)

    is_vtk = False
    if options.graphics == 'vedo':
        try:
            import hoofstats.plots_vtk as pl
            is_vtk = True

        except:
            import hoofstats.plots as pl

    else:
        import hoofstats.plots as pl

    cwd = os.path.realpath(os.path.curdir)
    output(f'working directory: {cwd}')

    wb = load_workbook(options.filename)
    sheet_name = (options.worksheet if options.worksheet is not None
                  else wb.sheetnames[0])
    ws = wb.get_sheet_by_name(sheet_name)
    output(f'using worksheet "{ws.title}" from {wb.sheetnames}')

    options.rows = [int(ii) if ii else None for ii in options.rows.split(':')]
    if options.rows[1] is None: options.rows[1] = ws.max_row

    if options.select_plot_measures is not None:
        options.select_plot_measures = [
            ii.strip('\t \'\"')
            for ii in options.select_plot_measures.split(',')
        ]

    data_description = Struct(**default_data_description)
    info = get_info(ws, data_description)
    output(info)

    output(f'using points in rows {options.rows}')

    pdesc, pcoors = get_points(ws, data_description, options.rows)

    if options.measures is not None:
        from importlib import import_module

        filename = os.path.realpath(options.measures)
        output(f'measures defined in: {filename}')

        path, _mod_name = os.path.split(filename)
        mod_name = os.path.splitext(_mod_name)[0]

        sys_path = sys.path
        sys.path = [path]
        mod = import_module(mod_name)
        sys.path = sys_path

        measures = eval_measures(pdesc, pcoors, mod.measures_description)

        output('evaluated measures:')
        output_measures(output, measures)

        save_measures(os.path.join(output_dir, 'measures.xlsx'),
                      measures, mod.measures_description)

    pl.plt.rcParams['font.size'] = 8
    pl.plt.rcParams['legend.handlelength'] = 2.5
    pl.plt.rcParams['legend.labelspacing'] = 0.0

    if is_vtk:
        points_fontsize = 2.5
        measures_fontsize = 3

    else:
        points_fontsize = 4
        measures_fontsize = 6

    ax = pl.plot_points(None, pcoors)
    if options.plot_point_names:
        ax = pl.plot_point_names(ax, pcoors, pdesc[:, 2],
                                 fontsize=points_fontsize)

    if options.measures is not None:
        if options.select_plot_measures is not None:
            measures = {key : val for key, val in measures.items()
                        if key in options.select_plot_measures}

        output('plotting measures:')
        output(list(measures.keys()))
        ax = pl.plot_measures(ax, measures, fontsize=measures_fontsize)

        if not is_vtk:
            if options.separate_legend:
                fig2 = pl.plt.figure()
                pl.put_legend_to_figure(fig2, ax)
                fig2.savefig(os.path.join(output_dir, 'points-legend.pdf'),
                             bbox_inches='tight')

            else:
                ax.legend()

    if not is_vtk:
        fig = ax.figure
        fig.savefig(os.path.join(output_dir, 'points.pdf'),
                    bbox_inches='tight')

    else:
        def _act_on_key(evt):
            if evt.keypress == 's':
                ax.screenshot(os.path.join(output_dir, 'points.pdf'))

        ax.add_callback('KeyPress', _act_on_key)

    if options.show:
        pl.show()

if __name__ == '__main__':
    main()
