import numpy as np
import matplotlib.pyplot as plt
import vedo as plv

def get_axes(ax, dim=None):
    if ax is None:
        ax = plv.Plotter(axes=4)

    return ax

def show():
    plv.show()

def plot_points(ax, coors):
    ax = get_axes(ax, coors.shape[1])
    ax += plv.Points(coors, r=8)

    return ax

def plot_point_names(ax, coors, names, **kwargs):
    ax = get_axes(ax, coors.shape[1])
    kwargs = kwargs.copy()
    fontsize = kwargs.pop('fontsize', None)
    for ii, name in enumerate(names):
        ax += plv.Text3D(name, coors[ii], s=fontsize, justify='centered',
                         alpha=0.5, **kwargs).followCamera()

    return ax

def plot_measures(ax, measures, fontsize=3):
    ax = get_axes(ax)

    colors = plt.cm.viridis(np.linspace(0, 1, len(measures)))
    labels = []
    for im, (name, measure) in enumerate(measures.items()):
        desc = measure[0]
        if desc in ('line', 'segment'):
            p0, p1 = measure[2], measure[3]
            ax += plv.Line(p0, p1, c=colors[im, :3], alpha=colors[im, 3], lw=3)
            ax += plv.Text3D(name, (0.7 * p0 + 0.3 * p1),
                             s=fontsize).followCamera()

            labels.append(f'|{name}| = {measure[-1]:.2f}')

        elif desc == 'point':
            pp = measure[2]
            ax += plv.Marker('x', pp, c=colors[im, :3], alpha=colors[im, 3],
                             s=2, filled=True)
            ax += plv.Text3D(name, pp, s=fontsize).followCamera()

            labels.append(f'point {name}')

        elif desc == 'plane':
            p0, p1, p2 = measure[2], measure[3], measure[4]
            ax += plv.Line((p0, p1, p2), c=colors[im, :3], alpha=colors[im, 3])
            ax += plv.Mesh([(p0, p1, p2), [(0, 1, 2)]], c=colors[im, :3],
                           alpha=0.1)
            ax += plv.Text3D(name, (p0 + p1 + p2) / 3.0,
                             s=fontsize).followCamera()

            labels.append(f'plane {name}')

        elif desc == 'polygon':
            ps = measure[2]
            ax += plv.Line(ps, c=colors[im, :3], alpha=colors[im, 3])
            ax += plv.Text3D(name, ps[0], s=fontsize).followCamera()

            labels.append(f'|{name}| = {measure[-1]:.2f}')

        elif desc == 'angle':
            pm0, pm1 = measure[2], measure[3]
            ax += plv.DashedLine(pm0, pm1, c=colors[im, :3],
                                 alpha=colors[im, 3])
            ax += plv.Text3D(name, 0.5 * pm0 + 0.5 * pm1,
                             s=fontsize).followCamera()

            labels.append(f'<{name} = {measure[-1]:.2f}')

        elif desc == 'area':
            pc, ps = measure[2], measure[3]

            ax += plv.DashedLine(ps, c=colors[im, :3], alpha=colors[im, 3],
                                 spacing=1)
            vertices = [pc] + ps.tolist()
            faces = []
            for ii in range(ps.shape[0] - 1):
                faces.append((0, ii, ii+1))
            ax += plv.Mesh([vertices, faces], c=colors[im, :3],
                           alpha=0.1)
            ax += plv.Text3D(name, pc, s=fontsize).followCamera()

            labels.append(f'||{name}|| = {measure[-1]:.2f}')

    return ax
