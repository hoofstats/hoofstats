hoofstats
=========

Hoof microscribe data statistics

Installation
------------

The source code of the development version in git:

```
git clone https://gitlab.com/hoofstats/hoofstats.git
cd hoofstats
pip install .
```

or the development version via pip:

```
pip install git+https://gitlab.com/hoofstats/hoofstats.git
```

Usage
-----

To get help, run:

```
hoofstats -h
```

This will print:

```
usage: hoofstats [-h] [-w worksheet-name] [-r rows-spec] [-m filename]
                 [--select-plot-measures name1[,name2,...]]
                 [--plot-point-names] [--separate-legend]
                 [--graphics {matplotlib,vedo}] [-n] [--silent] [-c] [-o path]
                 filename

Process microscribe data.

positional arguments:
  filename              microscribe data file name

options:
  -h, --help            show this help message and exit
  -w worksheet-name, --worksheet worksheet-name
                        worksheet name [default: first present]
  -r rows-spec, --rows rows-spec
                        point data rows range [default: 8:]
  -m filename, --measures filename
                        measures description file
  --select-plot-measures name1[,name2,...]
                        measures to plot. Plot all if not given.
  --plot-point-names    add point names to 3D plot
  --separate-legend     put legend into separate figure
  --graphics {matplotlib,vedo}
                        graphics backend. If vedo, press 's' to save a
                        screenshot of the current view [default: matplotlib]
  -n, --no-show         do not show the figures
  --silent              do not print messages to screen
  -c, --clear           clear old solution files from output directory
  -o path, --output-dir path
                        output directory [default: output]
```

Examples
--------

The following files are assumed to be in `data/`:

- `measurements.xlsx` contains the
  microscribe points and other information

- `measures.py` is the measures description file

Then `hoofstats` can be run as follows:

- Plot microscribe points:

  ```
  hoofstats data/measurements.xlsx
  ```

- Plot microscribe points and all measures, put legend to a separate figure:

  ```
  hoofstats data/measurements.xlsx -m data/measures.py --separate-legend
  ```

- Plot microscribe points and selected measures:

  ```
  hoofstats data/measurements.xlsx -m data/measures.py --select-plot-measures="'ak1 right', 'ak1 left', '_line8', '_plane1', '_line9'"
  ```

- Plot microscribe points, selected measures, and display point names:

  ```
  hoofstats data/measurements.xlsx -m data/measures.py --plot-point-names --select-plot-measures="'Hoof surface area', '_polygon1', '_polygon2', 'Right sole area', 'Left sole area'"
  ```

In all the examples above, the following results are stored in the default
output directory `output`:

- `measures.xlsx` - the evaluated measures.
- `points-legend.pdf` - the separate legend figure if `--separate-legend` was
  given.
- `points.pdf` - the points and (selected) measures plot.
